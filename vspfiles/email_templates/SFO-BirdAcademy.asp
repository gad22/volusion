<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<style type="text/css">body,table,td {font: 11px Verdana; color: #000000;}</style>
<body text="#000000">
<table width="650" border="1" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" bordercolor="#CCCCCC" align="center">
  <tr>
          <tr valign="top">
          <td><a href="https://store.birds.cornell.edu/">$(CompanyLogo)</a></td>
		  <td align="right">&nbsp;</td>
		</tr>

    <td><h3>Spring Field Ornithology</h3>
Dear Bird Enthusiast,
      <br />
      <br />
Thank you for your purchase of the Spring Field Ornithology--Northeast 2016 lectures and field trips. This 8 week course begins on March 23, 2016.
      <br />
      <br />
      The online component is hosted on the Cornell Lab's Bird Academy website and includes recorded lectures, downloadable handouts, and bird ID quizzes, available the day after each Wednesday evening lecture. You will automatically gain access to the material for each week as the course progresses. To help you continue to develop your skills, you can access the online content until Dec 1, 2016.
	  <br />
      <br />
Next steps:
1. To get started with the online portion of the course, please click here:
<a href="https://academy.allaboutbirds.org/enroll/spring-field-ornithology-northeast-2016/?enrollment_code=$(body)">https://academy.allaboutbirds.org/enroll/spring-field-ornithology-northeast-2016/?enrollment_code=$(body)</a>
      <br />
      <br />
      You can also enroll in the course by entering this enrollment code $(body) at the following link: <a href="https://academy.allaboutbirds.org/enroll/spring-field-ornithology-northeast-2016/">https://academy.allaboutbirds.org/enroll/spring-field-ornithology-northeast-2016/</a>
      <br />
      <br />
2. During enrollment, you will be asked to create a username and password so that you'll be able to return as often as you'd like. If you already have an account with the Cornell Lab, you do not need to create a new account.
     <br />
      <br />
SPECIAL FOR EARLY BIRDS: As a thank you for signing up before Feb. 7, we are offering free access to the Cornell Lab's self-paced online bird identification tutorials--a $58 value and great way to prepare for Spring Field Ornithology. To enroll for free, visit Be a <a href="https://academy.allaboutbirds.org/enroll/be-a-better-birder-size-and-shape/">Better Birder: Size & Shape</a> to enter the coupon code: betterbirder1-sfo-416 and <a href="https://academy.allaboutbirds.org/enroll/be-a-better-birder-color-and-pattern/">Be a Better Birder: Color & Pattern</a> to enter the coupon code: betterbirder2-sfo-416. You will be asked to create an account or sign-in using an existing Lab account (e.g., eBird).
    <br />
    <br />
As good preparation for this course, we invite you to view our free Inside Birding videos here: <a href="https://academy.allaboutbirds.org/inside-birding-color-pattern">https://academy.allaboutbirds.org/inside-birding-color-pattern</a>
      <br />
      <br />
For help please contact us at <a href="mailto:sfoclass@cornell.edu">sfoclass@cornell.edu</a>.      <br />
      <br />
Thank you,<br />
Marc Devokaitis<br />
Spring Field Ornithology Coordinator
      <br />
      <br />
Please note:
This online course is designed to work on all of the major modern web browsers (Internet Explorer, Chrome, Safari, and Firefox) as well as mobile devices including smartphones and tablets.
      <br />
      <br />
Access will expire Dec 1, 2016. Cornell University does not offer academic credit for this course.
      <br />
</td>
  </tr>
</table>
</body>
</html>
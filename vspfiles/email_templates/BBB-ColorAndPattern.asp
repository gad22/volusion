<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<style type="text/css">body,table,td {font: 11px Verdana; color: #000000;}</style>
<body text="#000000">
<table width="650" border="1" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" bordercolor="#CCCCCC" align="center">
  <tr>
          <tr valign="top"> 
          <td><a href="https://store.birds.cornell.edu/">$(CompanyLogo)</a></td>
		  <td align="right">&nbsp;</td>
		</tr>

    <td><h3>Be A Better Birder: Color & Pattern</h3>
Dear Customer,
      <br />
Thank you for your purchase of Be A Better Birder: Color & Pattern, the self-paced online tutorial from the Cornell Lab of Ornithology.
      <br />
      <br />
To access the course, please click here: 

<a href="http://academy.allaboutbirds.org/enroll/be-a-better-birder-color-and-pattern/?enrollment_code=$(body)">http://academy.allaboutbirds.org/enroll/be-a-better-birder-color-and-pattern/?enrollment_code=$(body)</a>
      <br />
      <br />
During enrollment, you will be asked to create a username and password so that you'll be able to return as often as you'd like. You can also enroll in the course by entering this enrollment code: $(body) at <a href="http://academy.allaboutbirds.org/enroll/be-a-better-birder-color-and-pattern/">http://academy.allaboutbirds.org/enroll/be-a-better-birder-color-and-pattern/</a>      <br />
      <br />
As good preparation for this course, we invite you to view our free Inside Birding videos here: <a href="http://academy.allaboutbirds.org/inside-birding-color-pattern">http://academy.allaboutbirds.org/inside-birding-color-pattern</a> 
      <br />
      <br />
For help please contact us at <a href="mailto:birdacademy@cornell.edu">birdacademy@cornell.edu</a>.
      <br />
      <br />
Thank you,<br />
The Cornell Lab of Ornithology-Education program
      <br />
      <br />
Please note:
This self-paced course is designed to work on all of the major modern web browsers (Internet Explorer, Chrome, Safari, and Firefox). iPhones and iPads will not show the Flash-based interactive elements.
      <br />
      <br />
      You can come back to this tutorial as much as you like. We do not guarantee that we will maintain the site forever, but you will retain access as long as it remains online.
      <br />
      <br />
      You will receive a certificate for completing the tutorial and passing a final quiz. Cornell University does not offer academic credit for this tutorial.
      <br />
      <br />Need help? Email <a href="mailto:birdacademy@cornell.edu">birdacademy@cornell.edu</a>
</td>
  </tr>
</table>
</body>
</html>
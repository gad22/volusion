<td><h3>Understanding Bird Behavior Webinars</h3>
      <br />
      Thank you for purchasing "Understanding Bird Behavior" webinar. Please save this email and use it to access the live event.
      <br />
      <br />
            To watch this webinar:
            <ol>
$(body)
            <li>Fill out the name and address fields (only name and email address are required), then hit "Submit."</li>
            <li>This action will open or download the Network Recording Player and begin the presentation in a new window on your screen. <b>Do not close</b> the original browser window until the player has fully downloaded and opened. Say "yes" to downloading the player if you do not already have it on your computer.</li>
            <li>At the end the webinar you will be directed to an exit survey in another window. Please take the time to give us your comments.</li>
         
            </ol>
            Following these steps you will be able to access the webinar from any computer with Internet access. Please note that currently you cannot watch it on a tablet or other mobile device.
<br />
<br />
      <br />
      <br />
            If you encounter any problems accessing your webinar, please <a ref="mailto:birdacademy@cornell.edu">email us at birdacademy@cornell.edu</a> or call us at 607-254-8312.
      <br />
      <br />
            </td>
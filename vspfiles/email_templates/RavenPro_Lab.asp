<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<style type="text/css">body,table,td {font: 11px Verdana; color: #000000;}</style>
<body text="#000000">
<table width="650" border="1" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" bordercolor="#CCCCCC" align="center">
  <tr>
    <td><h3>Raven Pro instructional/lab license</h3>
      <br />
Thank you for purchasing Raven Pro.
      <br />      
      <br />
Your INSTRUCTIONAL/LAB license serial number is included below.  In order to use Raven Pro with this license, you must first download and install the demo version of the program if you have not already done so.  To download the demo, visit <a href="http://www.birds.cornell.edu/raven">http://www.birds.cornell.edu/raven</a>, click on the Downloads button on the left side of the web page, and then follow the directions for downloading that are included on that page.
      <br />      
      <br />
To register your copy of Raven Pro and enable all of Raven Pro's features, follow these steps:
      	<ol>
      	<li>
Launch Raven Pro.  
	</li>
	<li>
Copy the text of the serial number below, and paste it into the Registration GUI.  
      <br />      
      <br />
$(body) 
      <br />      
      <br />
	</li>
	<li>
Type your email address into the Registration GUI. 
	</li>
	<li>
Press the Register button to complete your registration.
	</li>
	</ol>
      <br />
Installation Tips
      <br />
      <br />
      	<ol>
      	<li>
Keep a copy of this email in case you need to re-install Raven.
	</li>
      	<li>
Windows -- We recommend installing Raven in the default path.  Installing in C:\Program Files will not work.
	</li>
      	<li>
OSX -- We recommend installing and registering Raven when logged in as an administrator.  
	</li>
	</ol>
      <br />
If you have questions about your license, please contact us at the email address listed below.
        <br />      
      <br />

The Raven Team <br />
Bioacoustics Research Program <br />
Cornell Lab of Ornithology <br />
raven_orders@cornell.edu <br />

	</td>
  </tr>
</table>
</body>
</html>
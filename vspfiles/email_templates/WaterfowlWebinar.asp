<td><h3>Waterfowl ID Webinars</h3>
      <br />
      Thank you for purchasing a recorded webinar in our Waterfowl ID series. We are sorry if you could not attend the live event, but appreciate your interest. You will have access to this webinar recording until the end of this calendar year.
      <br />
      <br />
            To watch this webinar:
            <ol>
$(body)
            <li>Fill out the name and address fields (only name and email address are required), then hit "Submit."</li>
            <li>This action will open or download the Network Recording Player and begin the presentation in a new window on your screen. <b>Do not close</b> the original browser window until the player has fully downloaded and opened. Say "yes" to downloading the player if you do not already have it on your computer.</li>
            <li>Use the controls at the bottom of the Network Recording Player to start, stop, or rewind the recording.  Use the controls at the top of the screen, under "View" to open or close the video and polling windows.</li>
            <li>To end the webinar, use the "File-exit" command in the command bar at the top of the screen, or click the X in the corner of the window. You will be directed to an exit survey in another window. Please take the time to give us your comments.</li>
            </ol>
            Following these steps you will be able to access the webinar from any computer with Internet access. Please note that currently you cannot watch it on a tablet or other mobile device.
<br />
<br />
The handouts for all of our webinars can be <a href="http://www.birds.cornell.edu/courses/home/webinars/#waterfowl">downloaded as PDFs from our web site.</a>
      <br />
      <br />
            If you encounter any problems accessing your webinar, please <a ref="mailto:hstudy@cornell.edu">email us at hstudy@cornell.edu</a> or call us at 607-254-2452. If you encounter any technical issues, please call Cisco at 1-866-229-3239 to help with Event Center Recording playback.
      <br />
      <br />
            </td>
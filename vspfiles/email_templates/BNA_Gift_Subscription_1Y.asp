<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<style type="text/css">body,table,td {font: 11px Verdana; color: #000000;}</style>
<body text="#000000">
<table width="650" border="1" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" bordercolor="#CCCCCC" align="center">
  <tr>
    <td><h3>Birds of North America Online Gift Subscription Notification!</h3>
      <br />
This note confirms that you have received a gift subscription to the Cornell Lab of Ornithology&#8217;s renowned bird life history resource, Birds of North America Online! As you may know, the <a href="http://bna.birds.cornell.edu/bna">Birds of North America Online</a> (http://bna.birds.cornell.edu/bna) will provide you with Internet access to in-depth life history information for over 700 species of birds that regularly nest in the United States and Canada. In addition to text, photographs and videos of the species of interest, there are also recordings of selected vocalizations from the Cornell Lab of Ornithology&#8217;s Macaulay Library.
      <br />
      <br />
Before you can use BNA Online, you will need to activate your subscription by following the instructions below. Once activated, your subscription will give you complete access to BNA Online for
one year.

      <br />
      <br />
      	To activate your subscription:
      	<ol>
      	<li>
Click on the following link or visit the following url to navigate to the Birds of North America Online Account Set-up page where you can create a user account and enter your subscription code: <a href="http://bna.birds.cornell.edu/gift">setup your account and activate your subscription</a> (http://bna.birds.cornell.edu/gift)
      	</li>
      	<li>Fill out the User Account Information fields (a copy will be emailed to you for your records), then at the bottom of the screen...</li>
      	<li>Enter the following code in the 'Subscription Code' field: <b>$(body)</b></li>
      	<li>Click on the "Continue" button.</li>
      	</ol>
After following these steps you will be able to access the entire BNA Online database from any computer with Internet access. To access the site the next time, simply Sign In from the <a href="http://bna.birds.cornell.edu/bna">BNA Online home page</a> (http://bna.birds.cornell.edu/bna) with your user name and password. Please note that user names and passwords in BNA are both case and space sensitive.       <br />
      <br />
      We hope you enjoy your gift subscription to Birds of North America Online and if you encounter any problems activating your subscription, please email us at <a ref="mailto:bna-sales@cornell.edu">bna-sales@cornell.edu</a> or call us at 877-873-2626.
      <br />
      <br />
	</td>
  </tr>
</table>
</body>
</html>
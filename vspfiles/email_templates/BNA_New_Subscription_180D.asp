<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<style type="text/css">body,table,td {font: 11px Verdana; color: #000000;}</style>
<body text="#000000">
<table width="650" border="1" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" bordercolor="#CCCCCC" align="center">
  <tr>
    <td><h3>Birds of North America Online Subscription</h3>
      <br />
      Thank you for purchasing a Birds of North America Online subscription. Before you can use BNA Online, you will need to activate your subscription by following the instructions below. Once activated, your subscription will give you complete access to BNA Online for 
      180 days.
      <br />
      <br />
      	To activate your subscription:
      	<ol>
      	<li>Click on the following link to navigate to the Birds of North America Online Account Set-up page where you can create a user account and enter your subscription code: <a href="http://bna.birds.cornell.edu/activate-new">setup your account and activate your subscription (http://bna.birds.cornell.edu/activate-new)</a>
      	</li>
      	<li>Fill out the User Account Information fields (a copy will be mailed to you for your records), then at the bottom of the screen...</li>
      	<li>Enter the following code in the "Subscription Code" field: <b>$(body)</b></li>
      	<li>Click on the "Continue" button.</li>
      	</ol>
      	After following these steps you will be able to access the entire BNA Online database from any computer with Internet access. To access the site the next time, simply Sign In from the <a href="http://bna.birds.cornell.edu/bna/">BNA Onlne home page (http://bna.birds.cornell.edu/bna/)</a> with your user name and password. Please note that user names and passwords in BNA are both case and space sensitive.
      <br />
      <br />
      	If you encounter any problems activating your subscription, please <a ref="mailto:bna-sales@cornell.edu">email us at bna-sales@cornell.edu</a> or call us at 877-873-2626.
      <br />
      <br />
	</td>
  </tr>
</table>
</body>
</html>
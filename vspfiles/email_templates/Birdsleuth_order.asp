<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<style type="text/css">body,table,td {font: 11px Verdana; color: #000000;}</style>
<body text="#000000">
<table width="650" border="1" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" bordercolor="#CCCCCC" align="center">
  <tr>
    <td><a href="../../Default.asp">$(CompanyLogo)</a><br />
      <br />
      Dear $(Bill_FirstName)&nbsp;$(Bill_LastName),
Thank you for your BirdSleuth purchase, your order has been received.  
<br /><br />
The BirdSleuth Shop is located at Sapsucker Woods on the Cornell University campus 
and does not function like a major online retailer. Please allow 2-3 weeks for your 
order to process and ship. If you have any questions, please feel free to contact me at 
LD85@cornell.edu. 
		<br /><br />
Sincerely,<br /><br />
Lisa DeRado<br />
K-12 Education Resources Coordinator<br />
Cornell Lab of Ornithology<br />
	</td>
  </tr>
</table>
</body>
</html>
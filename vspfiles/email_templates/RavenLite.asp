<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<style type="text/css">body,table,td {font: 11px Verdana; color: #000000;}</style>
<body text="#000000">
<table width="650" border="1" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" bordercolor="#CCCCCC" align="center">
  <tr>
    <td><h3>Raven Lite 1.0 standard license</h3>
      <br />
Thank you for requesting a free license for Raven Lite 1.0.
      <br />      
      <br />
Your Raven Lite 1.0 license serial number is included below. In order to use Raven Lite
1.0 with this license, you must first download and install the demo version
of the program if you have not already done so. To download the demo, visit
http://www.birds.cornell.edu/raven, click on the Downloads button on the
left side of the web page, and then follow the directions for downloading
that are included on that page.

      <br />      
      <br />
To register your copy of Raven Lite 1.0 and enable all of Raven Lite's
features, follow these steps:

      	<ol>
      	<li>
Launch Raven Lite.
	</li>
	<li>
Copy the text of the serial number below, and paste it into the Registration GUI.  Then add your email address to the end of the serial number that you just pasted.
      <br />      
      <br />
$(body)InsertYourEmailAddressHere
      <br />      
      <br />
	</li>
	<li>
Press the Register button to complete your registration.
	</li>
	</ol>
      <br />
Installation Tips
      <br />
      	<ol>
      	<li>
Keep a copy of this email in case you need to re-install Raven.
	</li>
      	<li>
Windows -- We recommend installing Raven in the default path.  Installing in C:\Program Files will not work.
	</li>
      	<li>
OSX -- We recommend installing and registering Raven when logged in as an administrator.  
	</li>
	</ol>
      <br />

For assistance and feedback, please visit our HELP forum at:
http://help.RavenSoundSoftware.com/.
If you have questions about your license, please contact us at the email address listed below.
        <br />      
      <br />

The Raven Team <br />
Bioacoustics Research Program <br />
Cornell Lab of Ornithology <br />
raven_orders@cornell.edu <br />

	</td>
  </tr>
</table>
</body>
</html>
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

</head>

<style type="text/css">body,table,td {font: 11px Verdana; color: #000000;}</style>

<body text="#000000">

<table width="650" border="1" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" bordercolor="#CCCCCC" align="center">

  <tr>

          <tr valign="top"> 

          <td><a href="https://store.birds.cornell.edu/">$(CompanyLogo)</a></td>

		  <td align="right">&nbsp;</td>

		</tr>



    <td><h3>Enroll in the Home Study Course in Bird Biology</h3>

Dear Customer,

      <br />

Thank you for your purchase of the Home Study Course in Bird Biology, the online course from the Cornell Lab of Ornithology. You will need to have a copy of the Handbook of Bird Biology, 2nd edition to study for the online exams and complete the course.

      <br />

      <br />

To access the online exams, please click here: 



<a href="http://biology.allaboutbirds.org/enroll/home-study-course-in-bird-biology/?enrollment_code=$(body)">http://biology.allaboutbirds.org/enroll/home-study-course-in-bird-biology/?enrollment_code=$(body)</a>

      <br />

      <br />

During enrollment, you will be asked to create a username and password so that you'll be able to return as often as you'd like. You can also enroll in the course by entering this enrollment code: $(body) at <a href="http://biology.allaboutbirds.org/enroll/home-study-course-in-bird-biology/">http://biology.allaboutbirds.org/enroll/home-study-course-in-bird-biology/</a>      <br />

      <br />

      <br />

For help please contact us at <a href="mailto:bird-courses@cornell.edu">bird-courses@cornell.edu</a>.

      <br />

      <br />

Thank you for your interest in birds,<br />

The Cornell Lab of Ornithology-Education program

      <br />

      <br />

Please note:

This self-paced course is designed to work on all of the major modern web browsers (Internet Explorer, Chrome, Safari, and Firefox). iPhones and iPads will not show the Flash-based interactive elements.

      <br />

      <br />

      Although you will only be allowed to take each exam once, you can come back as many times as you like to finish the material. We do not guarantee that we will maintain the site forever, but you will retain access as long as it remains online. 

      <br />

      <br />

      You can request a certificate for passing the course. Cornell University does not offer academic credit for this course. 

      <br />

      <br />Need help? Email <a href="mailto:bird-courses@cornell.edu">bird-courses@cornell.edu</a>

</td>

  </tr>

</table>

</body>

</html>
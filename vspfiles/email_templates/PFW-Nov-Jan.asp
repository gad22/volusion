<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<style type="text/css">body,table,td {font: 11px Verdana; color: #000000;}</style>
<body text="#000000">
<table width="650" border="1" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" bordercolor="#CCCCCC" align="center">
  <tr>
    <td><a href="../../Default.asp">$(CompanyLogo)</a><br />
      <br />
		Thank you for supporting Project FeederWatch. 
If you registered yourself for Project FeederWatch, you are now signed up for the current season, which runs from <b>November 9, 2013, through April 4, 2014</b> (gift givers, see paragraph below). 
		<ul>
			<li><b>Your research kit</b> with your <b>ID number</b>, which is needed for access to online data entry, will be mailed to you <b>in 3-4 weeks</b>, unless you are a renewing participant and chose to receive no kit.
			</li>
            <li><b>Renewing participants</b>, please note that it may take <b>a couple of weeks</b> for your <b>ID number</b> to be <b>activated online</b>, even if you requested no kit.</li>
			<li>We have subscribed this email address to Project FeederWatch's <a href="http://www.birds.cornell.edu/pfw/ElectronicNewsletter.htm">electronic newsletter</a>, unless it was previously unsubscribed from the eNews subscription list. You can learn more about the newsletter and how to manage your subscription on our <a href="http://www.birds.cornell.edu/pfw/ElectronicNewsletter.htm">website</a>.
			</li>
			<li>You will receive the Lab's quarterly newsletter, <i>Living Bird News</i>, starting with the next issue.</li>
		</ul>
		<br /><br />
		If you are <b>giving Project FeederWatch as a gift</b>, we will send the gift recipient a kit in a couple of weeks with a postcard enclosed to let them know that you signed them up. They will also receive the Lab's quarterly newsletter, <i>Living Bird News</i>, starting with the next issue. If they would like to subscribe to our <a href="http://www.birds.cornell.edu/pfw/ElectronicNewsletter.htm">electronic newsletter</a>, they can learn more about the newsletter and how to manage their subscription on our <a href="http://www.birds.cornell.edu/pfw/ElectronicNewsletter.htm">website</a>. You may download and print a gift recipient notification certificate by clicking this <a href="http://feederwatch.org/gift-recipient-notification-certificate/">link</a>. We recommend that you forward this message to them.
	</td>
  </tr>
</table>
</body>
</html>
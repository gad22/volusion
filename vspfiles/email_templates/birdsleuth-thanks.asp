<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<style type="text/css">body,table,td {font: 11px Verdana; color: #000000;}</style>
<body text="#000000">
<table width="650" border="1" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" bordercolor="#CCCCCC" align="center">
  <tr>
    <td><a href="../../Default.asp">$(CompanyLogo)</a><br />
      <p>
		Thank you for your purchase from BirdSleuth K-12 at the Cornell Lab of Ornithology. 
		</p>
<p>While most BirdSleuth K-12 products are shipped from our offices in Ithaca, NY our <i>Habitat Connections</i> and <i>Owl Pellet Kits</i> are shipped from our distribution site in Hanover, PA. Please expect a total of 2-3 weeks shipping time to receive all components of your order.
</p>
		<p>
		 If you have questions, concerns or need further information about invoices and refunds you may contact Lisa DeRado at ld85@cornell.edu. The BirdSleuth K-12 store operates during business hours only, Monday-Friday, so please allow 1-2 business days for a response.
You can learn more, connect to like-minded educators, and keep track of special opportunities such as webinars, workshops, and contests by following us on <a href="http://www.facebook.com/pages/BirdSleuth/40097433976">facebook</a>, <a href="http://twitter.com/BirdSleuth">twitter</a>, or joining our <a href="http://www.birdsleuth.org/">mailing list</a>.</p>
<p>Sincerely,
<br /><br />
The BirdSleuth K-12 Team
</p>
</td>
  </tr>
</table>
</body>
</html>
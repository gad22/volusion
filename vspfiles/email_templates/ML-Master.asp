<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<style type="text/css">body,table,td {font: 11px Verdana; color: #000000;}</style>
<body text="#000000">
<table width="650" border="1" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" bordercolor="#CCCCCC" align="center">
  <tr>
    <td><h3>Macaulay Library Audio Guide Download</h3>
Dear Customer,
      <br />
Thank your for your purchase of a Cornell Lab of Ornithology audio guide. To download your file, please click here:
      <br />
      <br />
<a href="$(body)">Bird Sounds Download</a>
      <br />
      <br />
      Please note that this download link is a unique URL that is tied to your purchase. It cannot be shared or published. <b>This link will expire on February 1, 2016.</b>
      <br />
      <br />
After downloading your Cornell Lab audio guide, you will need to unzip the file. Once unzipped, the folder will contain the MP3s along with a digital booklet that will give you more information about the production. If you do not know how to unzip files or load the MP3s to your device, there are many resources on the web, or you can contact us at <a href="mailto:macaulaylibrary@cornell.edu">macaulaylibrary@cornell.edu</a> for detailed instructions.
      <br />
      <br />
With over 185,000 audio and 50,000 video recordings representing over 9,000 species, the Macaulay Library at the Cornell Lab of Ornithology is the world's largest archive of animal behavior. Since 1930, recordists of all backgrounds have contributed their recordings to build this unparalleled resource. Its mission is to collect and preserve recordings of each species behavior and natural history and to make them available for research, education, conservation, and the arts. Over 100,000 audio and 40,000 video recordings are now digitized and playable online at <a href="http://macaulaylibrary.org">macaulaylibrary.org</a>.
      <br />
      <br />
The Cornell Lab of Ornithology represents more than 40,000 supporters. Your support furthers the Lab's mission to protect the earth's wildlife through research, education, and citizen science focused on birds. Visit <a href="http://www.birds.cornell.edu/membership">www.birds.cornell.edu/membership</a> to find out how you can join and help conserve the birds that touch our lives and enrich our planet.
      <br />
      <br />
Sincerely,<br />
The Cornell Lab of Ornithology-Macaulay Library
	</td>
  </tr>
</table>
</body>
</html>
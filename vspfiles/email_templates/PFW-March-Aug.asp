<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<style type="text/css">body,table,td {font: 11px Verdana; color: #000000;}</style>
<body text="#000000">
<table width="650" border="1" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" bordercolor="#CCCCCC" align="center">
  <tr>
    <td><a href="../../Default.asp">$(CompanyLogo)</a><br />
      <br />
		Thank you for supporting Project FeederWatch. If you registered yourself for Project FeederWatch, you are now signed up for the next season, which runs from <b>November 8, 2014, through April 3, 2015</b> (gift givers, see paragraph below). 
		<ul>
			<li>Your <b>research kit</b> with your ID number will be <b>mailed to you in the fall</b>, unless you are a renewing participant and chose to receive no kit. If your kit does not arrive by November 1, please let us know.
			</li>
			<li>We have <b>subscribed this email address</b> to Project FeederWatch's <a href="http://feederwatch.org/about/enews-archive/">electronic newsletter</a>, unless it was previously unsubscribed from the <b>eNews subscription</b> list. You can learn more about the newsletter and how to manage your subscription on our <a href="http://feederwatch.org/about/enews-archive/">website</a>.
			</li> 
                         <li>You will receive the Lab's quarterly newsletter, starting with the next issue.
			</li>
		</ul>
		If you are giving <b>Project FeederWatch as a gift</b>, we will send the gift recipient a postcard in a couple of weeks to let them know that you signed them up, we will send them a kit in the fall, and they will receive the Lab's quarterly newsletter, starting with the next issue. If they would like to subscribe to our <a href="http://feederwatch.org/about/enews-archive/">electronic newsletter</a>, they can learn more about the newsletter and how to manage their subscription on our <a href="http://feederwatch.org/about/enews-archive/">website</a>. We recommend that you forward this message to them.
		<br /><br />
		<b>Data entry opens for the new season on November 1</b>. Until then, only participants registered for last season can enter that portion of our web site.
	</td>
  </tr>
</table>
</body>
</html>
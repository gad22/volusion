<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<style type="text/css">body,table,td {font: 11px Verdana; color: #000000;}</style>
<body text="#000000">
<table width="650" border="1" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" bordercolor="#CCCCCC" align="center">
  <tr>
    <td><h3>Birds of North America Online Subscription Renewal</h3>
      <br />
      Thank you for purchasing a renewal for your Birds of North America Online subscription. Please note that your subscription will not be renewed until you follow the activation steps listed below. Once the renewal is activated, the subscription will then be active for 
      30 days.
      <br />
      <br />
      	To activate your subscription:
      	<ol>
      	<li>Click on the following link to navigate to the BNA Online page where you can login and enter your subscription code: <a href="http://bna.birds.cornell.edu/activate-renewal">activate your renewal subscription (http://bna.birds.cornell.edu/activate-renewal)</a>
      		<ul><li>Be aware that you will need your user name and password to activate your subscription. If you've forgotten, <a href="http://bna.birds.cornell.edu/reminder">click here to retrieve your user name or password (http://bna.birds.cornell.edu/reminder)</a>.</li></ul>
      	</li>
      	<li>Enter the following code in the "Subscription Code" field on that page: <b>$(body)</b></li>
      	<li>Click on the "Continue" button.</li>
      	</ol>
      	If you encounter any problems activating your renewal, please <a ref="mailto:bna-sales@cornell.edu">email us at bna-sales@cornell.edu</a> or call us at 877-873-2626.
      <br />
      <br />
	</td>
  </tr>
</table>
</body>
</html>
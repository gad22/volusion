<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<style type="text/css">body,table,td {font: 11px Verdana; color: #000000;}</style>
<body text="#000000">
<table width="650" border="1" cellspacing="0" cellpadding="20" bgcolor="#FFFFFF" bordercolor="#CCCCCC" align="center">
  <tr>
    <td><a href="../../Default.asp">$(CompanyLogo)</a><br />
      <br />
		Thank you for supporting Project FeederWatch. If you registered yourself for Project FeederWatch, you are now signed up for the current season, which <b>ends April 4, 2014</b> (gift givers, see paragraph below). 
		<ul>
			<li><b>Because it is so late in the FeederWatch season</b>, if you are signing up for Project FeederWatch for the <b>first time</b>, you will receive a note in your kit indicating that you are signed up for next season (at no additional charge), as well as for the remainder of this season. 
			<li>Your <b>research kit</b> with your ID number will be mailed to you <b>in two or three weeks</b>, unless you are a renewing participant and chose to receive no kit. <b>Renewing participants,</b> please note that it may take <b>three weeks</b> for your <b>ID number</b> to be <b>activated online,</b> even if you requested no kit.
			</li>
			<li>We have <b>subscribed this email address</b> to Project FeederWatch's <a href="http://feederwatch.org/about/enews-archive/">electronic newsletter</a>, unless it was previously unsubscribed from the eNews subscription list. You can learn more about the newsletter and how to manage your subscription on our <a href="http://feederwatch.org/about/enews-archive/">website</a>.
			</li> 
			<li>You will receive the <b>Lab's quarterly newsletter</b> starting with the next issue.
			</li>
		</ul>
		<br /><br />
		If you are <b>giving Project FeederWatch as a gift</b>, we will send the gift recipient a kit in a couple of weeks with a postcard enclosed to let them know that you signed them up. You may download and print a gift recipient notification certificate by clicking this <a href="http://feederwatch.org/gift-recipient-notification-certificate/">link</a>, if you wish to send your recipient a notification. <b>Because it is so late in the FeederWatch season</b>, if this recipient is new to Project FeederWatch, they will receive a note in their kit indicating that they are signed up for next season (at no additional charge), as well as for the remainder of this season. They will also receive the Lab's quarterly newsletter, starting with the next issue. If they would like to subscribe to our <a href="http://feederwatch.org/about/enews-archive/">electronic newsletter</a>, they can learn more about the newsletter and how to manage their subscription on our <a href="http://feederwatch.org/about/enews-archive/">website</a>.  We recommend that you forward this message to them.
</td>
  </tr>
</table>
</body>
</html>
_menuCloseDelay=0
_menuOpenDelay=0
_followSpeed=0
_followRate=0
_subOffsetTop=3
_subOffsetLeft=-15
_scrollAmount=3
_scrollDelay=20

with(navStyle1=new mm_style()){
offclass="nav";
onclass="nav nav_hover";
pageclass="nav nav_hover nav_selected";
bordercolor="#000000";
borderstyle="solid";
borderwidth=0;
separatorcolor="#F5F4F4";
separatorsize="1";
padding=3;
fontsize="12px";
fontstyle="normal";
fontweight="normal";
fontfamily="Arial";
image="/v/vspfiles/templates/106/images/Template/Menu1_Bullet.gif";
subimage="/v/vspfiles/templates/106/images/Template/Menu1_Bullet_End.gif";
subimagepadding="2";
subimageposition="bottom";
}

with(subNavStyle1=new mm_style()){
offclass="nav subnav"
onclass="nav nav_hover subnav subnav_hover"
pageclass="nav nav_hover subnav subnav_hover nav_selected subnav_selected"
bordercolor="#dedede";
borderstyle="solid";
borderwidth=1;
separatorcolor="#F5F4F4";
separatorsize="1";
padding=3;
fontsize="12px";
fontstyle="normal";
fontweight="normal";
fontfamily="Arial";
image="/v/vspfiles/templates/106/images/Template/Menu1_Bullet.gif";
subimage="/v/vspfiles/templates/106/images/Template/Menu1_Bullet_End.gif";
subimagepadding="2";
subimageposition="bottom";
}



with(navStyle2=new mm_style()){
offclass="nav";
onclass="nav nav_hover";
pageclass="nav nav_hover nav_selected";
bordercolor="#000000";
borderstyle="solid";
borderwidth=0;
separatorcolor="#F5F4F4";
separatorsize="1";
padding=3;
fontsize="12px";
fontstyle="normal";
fontweight="normal";
fontfamily="Arial";
image="/v/vspfiles/templates/106/images/Template/Menu2_Bullet.gif";
subimage="/v/vspfiles/templates/106/images/Template/Menu2_Bullet_End.gif";
subimagepadding="2";
subimageposition="bottom";
}

with(subNavStyle2=new mm_style()){
offclass="nav subnav"
onclass="nav nav_hover subnav subnav_hover"
pageclass="nav nav_hover subnav subnav_hover nav_selected subnav_selected"
bordercolor="#dedede";
borderstyle="solid";
borderwidth=1;
separatorcolor="#F5F4F4";
separatorsize="1";
padding=3;
fontsize="12px";
fontstyle="normal";
fontweight="normal";
fontfamily="Arial";
image="/v/vspfiles/templates/106/images/Template/Menu2_Bullet.gif";
subimage="/v/vspfiles/templates/106/images/Template/Menu2_Bullet_End.gif";
subimagepadding="2";
subimageposition="bottom";
}
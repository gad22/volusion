_subOffsetLeft=-15
_menuCloseDelay=0
_menuOpenDelay=0
_followSpeed=0
_followRate=0
_subOffsetTop=3
_subOffsetLeft=-1
_scrollAmount=3
_scrollDelay=20

with(navStyle1=new mm_style()){
offclass="nav";
onclass="nav_hover";
pageclass="nav_hover nav_selected";
bordercolor="#D6D6D6";
borderstyle="solid";
borderwidth=0;
separatorcolor="#D6D6D6";
separatorsize="1";
padding="6px";
fontsize="11px";
fontstyle="normal";
fontweight="normal";
fontfamily="Arial";
image="/v/vspfiles/templates/101/images/Template/left_nav_bullet.png";
subimage="/v/vspfiles/templates/101/images/Template/left_nav_bullet_end.png";
subimagepadding="10";
subimageposition="middle";
}

with(subNavStyle1=new mm_style()){
offclass="nav subnav"
onclass="nav_hover subnav_hover"
pageclass="nav_hover subnav_hover nav_selected subnav_selected"
bordercolor="#cccccc";
borderstyle="solid";
borderwidth=1;
separatorcolor="#cccccc";
separatorsize="1";
padding="5px";
fontsize="11px";
fontstyle="normal";
fontweight="normal";
fontfamily="Arial";
image="/v/vspfiles/templates/101/images/Template/left_nav_bullet.png";
subimage="/v/vspfiles/templates/101/images/Template/left_nav_bullet_end.png";
subimagepadding="10";
subimageposition="middle";
menubgcolor="#ffffff";
}


with(navStyle2=new mm_style()){
offclass="nav";
onclass="nav_hover";
pageclass="nav_hover nav_selected";
bordercolor="#D6D6D6";
borderstyle="solid";
borderwidth=0;
separatorcolor="#D6D6D6";
separatorsize="1";
padding="6px";
fontsize="11px";
fontstyle="normal";
fontweight="normal";
fontfamily="Arial";
image="/v/vspfiles/templates/101/images/Template/left_nav_bullet.png";
subimage="/v/vspfiles/templates/101/images/Template/left_nav_bullet_end.png";
subimagepadding="10";
subimageposition="middle";
}

with(subNavStyle2=new mm_style()){
offclass="nav subnav"
onclass="nav_hover subnav_hover"
pageclass="nav_hover subnav_hover nav_selected subnav_selected"
bordercolor="#cccccc";
borderstyle="solid";
borderwidth=1;
separatorcolor="#cccccc";
separatorsize="1";
padding="5px";
fontsize="11px";
fontstyle="normal";
fontweight="normal";
fontfamily="Arial";
image="/v/vspfiles/templates/101/images/Template/left_nav_bullet.png";
subimage="/v/vspfiles/templates/101/images/Template/left_nav_bullet_end.png";
subimagepadding="10";
subimageposition="middle";
menubgcolor="#ffffff";
}


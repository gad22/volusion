
function display_menu_1() {
  navStyle1.image="/v/vspfiles/templates/vnonprofit/images/Template/Menu1_Bullet.gif";
  navStyle1.subimage="/v/vspfiles/templates/vnonprofit/images/Template/Menu1_Bullet_End.gif";
  with(milonic=new menuname("m_v1p0")) {
    style=navStyle1;
    itemwidth="150";
    alwaysvisible=1;
    orientation="vertical";
    position="relative";
    aI("text=Charity Projects;url=SearchResults.asp?Cat=22;pagematch=[-_]s/22\\.htm$|searchresults\\.asp\\?cat=22$;");
    aI("text=Sponsor a Child;url=SearchResults.asp?Cat=21;pagematch=[-_]s/21\\.htm$|searchresults\\.asp\\?cat=21$;showmenu=m_v1p21;");
    aI("text=Donate Today;url=/donate.asp;pagematch=[-_]s/20\\.htm$|searchresults\\.asp\\?cat=20$;");
  }
  subNavStyle1.image="/v/vspfiles/templates/vnonprofit/images/Template/Menu1_Bullet.gif";
  subNavStyle1.subimage="/v/vspfiles/templates/vnonprofit/images/Template/Menu1_Bullet_End.gif";
  with(milonic=new menuname("m_v1p21")) {
    style=subNavStyle1;
    aI("text=New Category&nbsp;&nbsp;&nbsp;;url=SearchResults.asp?Cat=25;pagematch=[-_]s/25\\.htm$|searchresults\\.asp\\?cat=25$;");
    aI("text=New Category&nbsp;&nbsp;&nbsp;;url=SearchResults.asp?Cat=24;pagematch=[-_]s/24\\.htm$|searchresults\\.asp\\?cat=24$;");
  }
  drawMenus();
}


function display_menu_2() {
  navStyle2.image="/v/vspfiles/templates/vnonprofit/images/Template/Menu2_Bullet.gif";
  navStyle2.subimage="/v/vspfiles/templates/vnonprofit/images/Template/Menu2_Bullet_End.gif";
  with(milonic=new menuname("m_v2p0")) {
    style=navStyle2;
    itemwidth="150";
    alwaysvisible=1;
    orientation="vertical";
    position="relative";
    aI("text=How you can help;url=/MailingList_subscribe.asp;pagematch=[-_]s/17\\.htm$|searchresults\\.asp\\?cat=17$;");
    aI("text=Our Mission;url=/aboutus.asp;pagematch=[-_]s/12\\.htm$|searchresults\\.asp\\?cat=12$;showmenu=m_v2p12;");
  }
  subNavStyle2.image="/v/vspfiles/templates/vnonprofit/images/Template/Menu2_Bullet.gif";
  subNavStyle2.subimage="/v/vspfiles/templates/vnonprofit/images/Template/Menu2_Bullet_End.gif";
  with(milonic=new menuname("m_v2p12")) {
    style=subNavStyle2;
    aI("text=New Category&nbsp;&nbsp;&nbsp;;url=SearchResults.asp?Cat=27;pagematch=[-_]s/27\\.htm$|searchresults\\.asp\\?cat=27$;");
    aI("text=New Category&nbsp;&nbsp;&nbsp;;url=SearchResults.asp?Cat=26;pagematch=[-_]s/26\\.htm$|searchresults\\.asp\\?cat=26$;");
  }
  drawMenus();
}



function display_menu_1() {
  navStyle1.image=Config_CDN_URL + "v/vspfiles/templates/cornell/images/Template/Menu1_Bullet.gif";
  navStyle1.subimage=Config_CDN_URL + "v/vspfiles/templates/cornell/images/Template/Menu1_Bullet_End.gif";
  with(milonic=new menuname("m_v1p0")) {
    style=navStyle1;
    itemwidth="";
    alwaysvisible=1;
    orientation="horizontal";
    position="relative";
    aI("text=Birds of North America;url=http://store.birds.cornell.edu/Birds_of_North_America_s/47.htm;pagematch=[-_]s/47\\.htm$|searchresults\\.asp\\?cat=47$;");
    aI("text=BirdSleuth K-12;url=http://store.birds.cornell.edu/birdsleuth_s/22.htm;pagematch=[-_]s/22\\.htm$|searchresults\\.asp\\?cat=22$;showmenu=m_v1p22;");
    aI("text=Courses;url=http://store.birds.cornell.edu/category_s/31.htm;pagematch=[-_]s/31\\.htm$|searchresults\\.asp\\?cat=31$;showmenu=m_v1p31;");
    aI("text=Raven;url=http://store.birds.cornell.edu/Raven_s/20.htm;pagematch=[-_]s/20\\.htm$|searchresults\\.asp\\?cat=20$;");
    aI("text=Celebrate Urban Birds;url=http://store.birds.cornell.edu/category_s/57.htm;pagematch=[-_]s/57\\.htm$|searchresults\\.asp\\?cat=57$;");
    aI("text=Audio Guides;url=http://store.birds.cornell.edu/category_s/59.htm;pagematch=[-_]s/59\\.htm$|searchresults\\.asp\\?cat=59$;");
    aI("text=Gifts;url=http://store.birds.cornell.edu/category_s/60.htm;pagematch=[-_]s/60\\.htm$|searchresults\\.asp\\?cat=60$;");
  }
  subNavStyle1.image=Config_CDN_URL +"v/vspfiles/templates/cornell/images/Template/Menu1_Bullet.gif";
  subNavStyle1.subimage=Config_CDN_URL +"v/vspfiles/templates/cornell/images/Template/Menu1_Bullet_End.gif";
  with(milonic=new menuname("m_v1p22")) {
    style=subNavStyle1;
    aI("text=BirdSleuth Webinar Series&nbsp;&nbsp;&nbsp;;url=http://store.birds.cornell.edu/BirdSleuth_webinars_s/56.htm;pagematch=[-_]s/56\\.htm$|searchresults\\.asp\\?cat=56$;");
  }
  subNavStyle1.image=Config_CDN_URL +"v/vspfiles/templates/cornell/images/Template/Menu1_Bullet.gif";
  subNavStyle1.subimage=Config_CDN_URL +"v/vspfiles/templates/cornell/images/Template/Menu1_Bullet_End.gif";
  with(milonic=new menuname("m_v1p31")) {
    style=subNavStyle1;
    aI("text=Be a Better Birder Tutorials&nbsp;&nbsp;&nbsp;;url=http://store.birds.cornell.edu/category_s/62.htm;pagematch=[-_]s/62\\.htm$|searchresults\\.asp\\?cat=62$;");
    aI("text=Ornithology Webinars&nbsp;&nbsp;&nbsp;;url=http://store.birds.cornell.edu/category_s/55.htm;pagematch=[-_]s/55\\.htm$|searchresults\\.asp\\?cat=55$;");
    aI("text=Spring Field Ornithology&nbsp;&nbsp;&nbsp;;url=http://store.birds.cornell.edu/category_s/49.htm;pagematch=[-_]s/49\\.htm$|searchresults\\.asp\\?cat=49$;");
    aI("text=Home Study Course in Bird Biology&nbsp;&nbsp;&nbsp;;url=http://store.birds.cornell.edu/Home_Study_Course_in_Bird_Biology_s/44.htm;pagematch=[-_]s/44\\.htm$|searchresults\\.asp\\?cat=44$;");
  }
  drawMenus();
}

